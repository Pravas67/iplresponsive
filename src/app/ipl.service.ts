import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from './ipl/models/tournament.model';
import { TeamAmount } from './ipl/models/teamAmount.model';
import { Player } from './ipl/models/player.model';

@Injectable({
  providedIn: 'root'
})
export class IplService {
  base_url="localhost:8080//api/v1/team/";

  constructor(private http:HttpClient) { }

  getTeamDetails():Observable<Team[]>{
    console.log("getTeamDetails");
    return this.http.get<Team[]>(`${this.base_url}`);
  }
  getAmountSpentByTeams():Observable<TeamAmount[]>{
    return this.http.get<TeamAmount[]>(`localhost:8080//api/v1/stat/amountspentonrolebyteam`);
  }
  getAllPlayers():Observable<Player[]>{
    return this.http.get<Player[]>(`localhost:8080//api/v1/stat/allplayers`);
  }
  getMaxPaidPlayers():Observable<Player[]>{
    return this.http.get<Player[]>(`${this.base_url}maxamoutbyrole`);
  }
}
